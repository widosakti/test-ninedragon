const initialState = {
  isLoading: false,
  categoryId: -1
}
const blog = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_BLOG':
      console.log(action.payload)
      return {
        ...state,
        categoryId: action.payload
      }

    default:
      return state;
  }
}
export default blog;