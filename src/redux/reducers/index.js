import { combineReducers } from 'redux';
import category from './category';
import blog from './blog';
import login from './login';

const appReducer = combineReducers({
  category,
  blog,
  login
});

export default appReducer;