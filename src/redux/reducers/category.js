const initialState = {}
  const category = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_CUSTOMER':
      return {
        ...state,
        isLoading: true,
        customer: action.payload
      }
  
      default:
      return state;
    }
  }
  export default category;