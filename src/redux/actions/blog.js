export const getBlog = (category_id) => {
    return {
        type: 'GET_BLOG',
        payload: category_id
    }
}