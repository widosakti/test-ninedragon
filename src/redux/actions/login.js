import { PostAPI } from "../../helper/restapi"
export const getLogin = (body) => {
    return {
        type: 'GET_LOGIN',
        payload: PostAPI("/login", body)
    }
}