import axios from "axios";
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const GetAPI = (path) => {
    const token = cookies.get('token');
    return axios.get(process.env.REACT_APP_API + path, {
        headers: {
            Authorization: "Bearer " + token
        }
    })
}

const PostAPI = (path, body) => {
    const token = cookies.get('token');
    return axios.post(process.env.REACT_APP_API + path, body, {
        headers: {
            Authorization: "Bearer " + token
        }
    })
}

const PostFormAPI = (path, body) => {
    const token = cookies.get('token');
    return axios.post(process.env.REACT_APP_API + path, body, {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + token
        }
    })
}
export {
    GetAPI,
    PostAPI,
    PostFormAPI
}