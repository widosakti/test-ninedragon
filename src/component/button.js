const Button = (props) => {
    return (
        <button
            style={{
                ...props.style
            }}
            onClick={props.onClick}
        >
            <i className={props.icon} style={{ marginRight: 10}}></i>
            {props.title}
        </button>
    )
}
export default Button