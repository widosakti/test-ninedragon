import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import Login from './container/login';
import Register from './container/register';
import Category from './container/category';
import CreateCategory from './container/category/create';
import CreateBlog from './container/blog/create';
import Blog from './container/blog';
import Cookies from 'universal-cookie';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/store';

const cookies = new Cookies();

const LoginMiddleware = ({ children }) => {
  const token = cookies.get('token');
  if (token) {
    return children
  } else {
    return <Login />
  }
}

class Main extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Category />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/create-category" element={<CreateCategory />} />
          <Route path="/blog" element={<LoginMiddleware><Blog /></LoginMiddleware>} />
          <Route path="/create-blog" element={<LoginMiddleware><CreateBlog /></LoginMiddleware>} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Main />
        </PersistGate>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
