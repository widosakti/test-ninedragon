import React from "react";
import { GetAPI } from "../../helper/restapi";
import Main from "../../layout/main";
import { connect } from "react-redux";
import { useNavigate } from "react-router";
import { getBlog } from "../../redux/actions/blog"
import "./category.css";

const Category = (props) => {
    const [category, setCategory] = React.useState([])
    const navigate = useNavigate();

    React.useEffect(() => {
        if (category.length === 0) {
            getCategoryList()
        }
    }, [category])

    const getCategoryList = async () => {
        GetAPI("/get_category").then((res) => {
            setCategory(res.data.success);
        }).catch((error) => {
            console.log(error)
        })
    }

    const openBlog = async (item) => {
        await props.dispatch(getBlog(item.id));
        navigate("/blog");
    }

    return (
        <Main>
            <div style={{ display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                <ul>
                    {category.map((item) => (
                        <li onClick={() => openBlog(item)} className="card-container">{item.name}</li>
                    ))}
                </ul>
            </div>
        </Main>
    )
}

const mapStateToProps = ({ blog }) => {
    return {
        blog: blog
    }
}


export default connect(mapStateToProps)(Category)