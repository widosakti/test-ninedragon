import React from "react";
import Main from "../../layout/main";
import "./category.css";
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import { connect } from "react-redux";
import { PostAPI } from "../../helper/restapi";
import { useNavigate } from "react-router";
const CreateCategory = () => {
    const [category, setCategory] = React.useState('');
    const navigate = useNavigate();

    const handleChange = (event, name) => {
        setCategory(event.target.value);
    };

    const submit = async () => {
        try {
            let res = await PostAPI("/create_category", {
                name: category
            })
            if (res) {
                alert("Category success created");
                navigate("/");
            }
        } catch (error) {
            alert(error.response.data.error)
        }
    }
    return (
        <Main>
            <div className="container">
                <div className="login-container">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1 },
                        }}
                        noValidate
                        autoComplete="off"
                        className="form-container"
                    >
                        <FormControl style={{ width: "500px" }}>
                            <InputLabel htmlFor="component-outlined">Category Name</InputLabel>
                            <OutlinedInput
                                id="component-outlined"
                                value={category}
                                placeholder="Masukkan nama kategori"
                                onChange={(e) => handleChange(e, "category")}
                                label="Category Name"
                            />
                        </FormControl>
                        <Button onClick={() => submit()} variant="contained" style={{ width: "500px" }}>Create</Button>
                    </Box>
                </div>
            </div>
        </Main>
    )
}

const mapStateToProps = (state) => {
    return {
        login: state.login
    }
}

export default connect(mapStateToProps)(CreateCategory)