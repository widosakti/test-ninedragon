import React, { useEffect } from "react"
import Main from "../../layout/main";
import "./login.css";
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import { connect } from "react-redux";
import Cookies from 'universal-cookie';
import { PostAPI } from "../../helper/restapi";

const cookies = new Cookies();
const Login = (props) => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [token, setToken] = React.useState('');
    useEffect(() => {
        if (token === '') {
            setToken(cookies.get("token"));
        }
    }, [token])

    const handleChange = (event, name) => {
        if (name === "email") {
            setEmail(event.target.value);
        } else {
            setPassword(event.target.value);
        }
    };

    const submit = async () => {
        try {
            let res = await PostAPI("/login", {
                email: email,
                password: password
            })
            if (res) {
                cookies.set("token", res.data.success.token)
                setToken(res.data.success.token)
                window.location.reload();
            }
        } catch (error) {
            alert(error.response.data.error)
        }
    }

    return (
        <Main>
            <div className="container">
                <div className="login-container">
                    {
                        !token ?
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { m: 1 },
                                }}
                                noValidate
                                autoComplete="off"
                                className="form-container"
                            >
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Email</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={email}
                                        placeholder="Masukkan email anda"
                                        onChange={(e) => handleChange(e, "email")}
                                        label="Email"
                                    />
                                </FormControl>
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Password</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={password}
                                        placeholder="Masukkan password anda"
                                        onChange={(e) => handleChange(e, "password")}
                                        type="password"
                                        label="Password"
                                    />
                                </FormControl>
                                <Button onClick={() => submit()} variant="contained" style={{ width: "500px" }}>Login</Button>
                            </Box> :
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { m: 1 },
                                }}
                                noValidate
                                autoComplete="off"
                                className="form-container"
                            >
                                <Button onClick={() => {
                                    cookies.remove("token");
                                    setToken("");
                                }} variant="contained" style={{ width: "500px" }}>Logout</Button>
                            </Box>
                    }
                </div>
            </div>
        </Main>
    )
}

const mapStateToProps = (state) => {
    return {
        login: state.login
    }
}


export default connect(mapStateToProps)(Login)