import React from "react"
import Main from "../../layout/main";
import "./register.css";
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import { connect } from "react-redux";
import Cookies from 'universal-cookie';
import { PostFormAPI } from "../../helper/restapi";
import qs from 'qs'

const cookies = new Cookies();

const Register = () => {
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [c_password, setCPassword] = React.useState('');
    const [token, setToken] = React.useState('');
    React.useEffect(() => {
        if (token === '') {
            setToken(cookies.get("token"));
        }
    }, [token])
    const handleChange = (event, name) => {
        if (name === "email") {
            setEmail(event.target.value);
        } else if (name === "password") {
            setPassword(event.target.value);
        } else if (name === "c_password") {
            setCPassword(event.target.value);
        } else {
            setName(event.target.value);
        }
    };
    const submit = async () => {
        try {
            let res = await PostFormAPI("/register", qs.stringify({
                name: name,
                email: email,
                password: password,
                c_password: c_password
            }))
            if (res) {
                cookies.set("token", res.data.success.token)
                setToken(res.data.success.token)
            }
        } catch (error) {
            alert(error.response.data.error)
        }
    }
    return (
        <Main>
            <div className="container">
                <div className="login-container">
                    {
                        !token ?
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { m: 1 },
                                }}
                                noValidate
                                autoComplete="off"
                                className="form-container"
                            >
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Name</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={name}
                                        placeholder="Masukkan nama anda"
                                        onChange={(e) => handleChange(e, "name")}
                                        label="Name"
                                    />
                                </FormControl>
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Email</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={email}
                                        placeholder="Masukkan email anda"
                                        onChange={(e) => handleChange(e, "email")}
                                        label="Email"
                                    />
                                </FormControl>
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Password</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={password}
                                        placeholder="Masukkan password anda"
                                        onChange={(e) => handleChange(e, "password")}
                                        type="password"
                                        label="Password"
                                    />
                                </FormControl>
                                <FormControl style={{ width: "500px" }}>
                                    <InputLabel htmlFor="component-outlined">Confirm Password</InputLabel>
                                    <OutlinedInput
                                        id="component-outlined"
                                        value={c_password}
                                        placeholder="Masukkan password anda"
                                        onChange={(e) => handleChange(e, "c_password")}
                                        type="password"
                                        label="Confirm Password"
                                    />
                                </FormControl>
                                <Button onClick={() => submit()} variant="contained" style={{ width: "500px" }}>Register</Button>
                            </Box> :
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { m: 1 },
                                }}
                                noValidate
                                autoComplete="off"
                                className="form-container"
                            >
                                <Button onClick={() => {
                                    cookies.remove("token");
                                    setToken("");
                                }} variant="contained" style={{ width: "500px" }}>Logout</Button>
                            </Box>
                    }
                </div>
            </div>
        </Main>
    )
}

export default Register