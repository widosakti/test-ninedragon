import React from "react";
import Main from "../../layout/main";
import "./blog.css";
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import { connect } from "react-redux";
import { PostFormAPI } from "../../helper/restapi";
import { useNavigate } from "react-router";
import qs from 'qs'

const CreateBlog = (props) => {
    const [name, setName] = React.useState('');
    const [disc, setDisc] = React.useState('');
    const navigate = useNavigate();

    const handleChange = (event, name) => {
        if (name === "name") {
            setName(event.target.value);
        } else {
            setDisc(event.target.value);
        }
    };

    const submit = async () => {
        try {
            let res = await PostFormAPI("/create_blog", qs.stringify({
                id: 1,
                name: name,
                disc: disc,
                category_id: props.blog.categoryId
            }))
            if (res) {
                alert("Blog success created");
                navigate("/blog");
            }
        } catch (error) {
            alert(error.response.data.error)
        }
    }
    return (
        <Main>
            <div className="container">
                <div className="login-container">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1 },
                        }}
                        noValidate
                        autoComplete="off"
                        className="form-container"
                    >
                        <FormControl style={{ width: "500px" }}>
                            <InputLabel htmlFor="component-outlined">Blog Name</InputLabel>
                            <OutlinedInput
                                id="component-outlined"
                                value={name}
                                placeholder="Masukkan nama blog"
                                onChange={(e) => handleChange(e, "name")}
                                label="Blog Name"
                            />
                        </FormControl>
                        <FormControl style={{ width: "500px" }}>
                            <InputLabel htmlFor="component-outlined">Description Name</InputLabel>
                            <OutlinedInput
                                id="component-outlined"
                                value={disc}
                                placeholder="Masukkan nama descripsi"
                                onChange={(e) => handleChange(e, "disc")}
                                label="Description Name"
                            />
                        </FormControl>
                        <Button onClick={() => submit()} variant="contained" style={{ width: "500px" }}>Create Blog</Button>
                    </Box>
                </div>
            </div>
        </Main>
    )
}
const mapStateToProps = ({ blog }) => {
    return {
        blog: blog
    }
}

export default connect(mapStateToProps)(CreateBlog)