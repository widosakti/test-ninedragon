import React from "react";
import { GetAPI } from "../../helper/restapi";
import Main from "../../layout/main";
import { connect } from "react-redux";
import { useNavigate } from "react-router";
import Button from '@mui/material/Button';
import "./blog.css";

const Blog = (props) => {
    const [blog, setBlog] = React.useState([])
    const [isLoading, setLoading] = React.useState(false)
    const navigate = useNavigate();

    React.useEffect(() => {
        getBlogList()
    }, [])

    const getBlogList = async () => {
        try {
            setLoading(true);
            let res = await GetAPI("/get_blog");
            if (res) {
                let tempArr = [];
                res.data.success.map((item) => {
                    console.log(item.category_id)
                    if (Number(item.category_id) === Number(props.blog.categoryId)) {
                        tempArr.push(item)
                    }
                })
                setBlog(tempArr);
                setLoading(false);
            }
        } catch (error) {
            console.log(error)
        }
    }

    const createBlog = () => {
        navigate("/create-blog")
    }

    if (isLoading) {
        return (
            <Main>
                <Button onClick={() => createBlog()} variant="contained" style={{ position: "absolute", top: "280px", right: "100px" }}>
                    Create Blog
                </Button>
                <div style={{ display: "grid", justifyContent: "center", marginTop: "100px" }}>
                    <h1>Loading...</h1>
                </div>
            </Main>
        )
    }

    return (
        <Main>
            <Button onClick={() => createBlog()} variant="contained" style={{ position: "absolute", top: "280px", right: "100px" }}>
                Create Blog
            </Button>
            <div style={{ display: "grid", justifyContent: "center", marginTop: "100px" }}>
                {
                    blog.length === 0 ? <h1>Tidak ada Blog yang tersedia</h1> :
                        <ul className="blog-container">
                            {blog.map((item) => (
                                <div className="card-container-blog">
                                    <h3>{item.name}</h3>
                                    <span>{item.disc}</span>
                                </div>
                            ))}
                        </ul>
                }
            </div>
        </Main>
    )
}

const mapStateToProps = ({ blog }) => {
    return {
        blog: blog
    }
}


export default connect(mapStateToProps)(Blog)