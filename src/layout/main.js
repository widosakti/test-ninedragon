import "./main.css"
import { useLocation, useNavigate } from "react-router-dom"

const Main = ({ children }) => {
    const location = useLocation();
    const navigate = useNavigate();
    return (
        <>
            <div className="top-banner">
                BLOG FOR YOUR IDEAS
            </div>
            <div class="navbar">
                <a class={location.pathname === "/" ? "active" : ""} onClick={() => navigate("/")}><i class="fa fa-fw fa-home"></i> Category</a>
                <a class={location.pathname === "/create-category" ? "active" : ""} onClick={() => navigate("/create-category")}><i class="fa fa-fw fa-plus"></i> Create Category</a>
                <a class={location.pathname === "/register" ? "active" : ""} onClick={() => navigate("/register")}><i class="fa fa-fw fa-user-plus"></i> Register</a>
                <a className={location.pathname === "/login" ? "active" : ""} onClick={() => navigate("/login")}><i class="fa fa-fw fa-user"></i> Login</a>
            </div>
            {children}
        </>
    )
}

export default Main